Pod::Spec.new do |s|
  s.name          = "DemoSDK"
  s.version       = "0.0.1"
  s.summary       = "iOS SDK for Hello World"
  s.description   = "iOS SDK for Hello World, including example app"
  s.homepage      = "https://akashkt09@bitbucket.org/akashkt09/"
  s.license       = "MIT"
  s.author        = "akashkt09"
  s.platform      = :ios, "13.0"
  s.swift_version = "4.2"
  s.source        = {
    :git => "https://akashkt09@bitbucket.org/akashkt09/demosdk.git",
    :tag => "#{s.version}"
  }
  s.source_files        = "DemoSDK/**/*.{h,m,swift}"
  s.public_header_files = "DemoSDK/**/*.h"
end

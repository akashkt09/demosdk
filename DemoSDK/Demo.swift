//
//  Demo.swift
//  DemoSDK
//
//  Created by Akash on 23/12/21.
//

import Foundation


public class HelloWorld {
    let hello = "Hello"

    public init() {}

    public func hello(to whom: String) -> String {
        return "Hello \(whom)"
    }
}
